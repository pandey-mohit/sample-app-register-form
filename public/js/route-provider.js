
/*
 * defining angular routes
 * */
angular.module('routeProvider', []).config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
  $routeProvider
    // register page
    .when('/register', {
      templateUrl: 'views/register.html',
      controller: 'registerController'
    })
    .when('/search', {
      templateUrl: 'views/search.html',
      controller: 'searchController'
    })
    .otherwise({
      redirectTo: '/register'
    });

  $locationProvider.html5Mode(true);

}]);