
/*
 * handling API calls using $http
 * */
angular.module('services', []).factory('service', ['$http', function($http) {
  return {
    // call to register people
    register: function(data){
      return $http.post('/api/register', data);
    },
    // call to search people
    search: function(data){
      return $http.post('/api/search', data);
    }
  }
}]);