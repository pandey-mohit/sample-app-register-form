
/*
 * application start point
 * starting up all required component using dependency injection
 * */
angular.module('sampleApp', ['ngRoute', 'routeProvider', 'registerController', 'searchController']);