
/*
 * search controller
 * */
angular.module('searchController', ['ui.bootstrap', 'services'])
  .controller('searchController',['$scope', 'service', function($scope, service) {
  $scope.getPeople = getPeople;
  $scope.onSelect = onSelect;
  $scope.searchedPerson = [];

  function getPeople(value){
    return service.search({ name: value }).
      then(function(response) {
        if(response.data && response.data.success == 1){
          return response.data.message;
        } else {
          return [];
        }
      });
  }

  function onSelect(item) {
    $scope.searchedPerson = [item];
  }

}]);