
/*
 * register controller
 * */
angular.module('registerController', ['ui.bootstrap', 'angular-flash.service', 'angular-flash.flash-alert-directive', 'services'])
  .controller('registerController',['$scope', 'service', 'flash', function($scope, service, flash) {
  $scope.user = {};
  $scope.submit = submit;

  function submit(){
    service.register($scope.user).
      success(function(data, status, headers, config) {
        if(data.success == 1) {
          $scope.form.$setPristine();
          $scope.user = {};
          flash.success = data.message;
        } else {
          flash.error = data.message;
        }
      }).
      error(function(data, status, headers, config) {
        flash.error = data.message;
      });
  }

}]);