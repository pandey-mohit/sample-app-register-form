
/*
 * db connection and model definition
 * */

// grab the mongoose module
var mongoose = require('mongoose');
// grab config file
var config = require('./config');

// mongoose connection to mongodb
mongoose.connect(config.url, function (error) {
  if (error) {
    console.log(error);
  }
});
mongoose.connection.on('error', console.error.bind(console, 'connection error:'));
mongoose.connection.once('open', function callback () {
  console.log('connection open');
});
mongoose.connection.on('disconnected', function () {
  mongoose.connect(config.url);
})

// mongoose schema definition
var mongooseSchema = mongoose.Schema;
var registerSchema = new mongooseSchema({
  name: {type : String, default: ''},
  address: {type : String, default: ''},
  area: {type : String, default: ''},
  city: {type : String, default: ''},
  phoneNumber: {type : String, default: ''}
});

module.exports = {
  register: mongoose.model('register', registerSchema)
}

