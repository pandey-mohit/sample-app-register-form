
/*
 * node API to handle calls made from frontend(angular)
 * */

// grab the model we just created
var model = require('./db-model');

module.exports = function(app) {
  // sample api route
  app.post('/api/register', function(req, res) {
    model.register.create(req.body, function(error, response) {
      if (error)
        res.send({success:0, message: error});
      res.send({success:1, message: 'User registered successfully.'});
    });
  });
  app.post('/api/search', function(req, res) {
    model.register.find({name: new RegExp('' + req.body.name + '', "i")}, function(error, response) {
      if (error)
        res.send({success:0, message: error});
      res.send({success:1, message: response});
    });
  });

  app.get('*', function(req, res) {
    res.sendfile('./public/index.html');
  });

};